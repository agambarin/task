<?php

namespace components;

class DbConnection
{
    protected $dbh;

    private static $instance;

    protected $config = [];

    private function __construct()
    {
        $this->config = require __DIR__ . '/../config.php';
    }

    private function __wakeup()
    {
    }

    /**
     * @return static
     */
    public static function getInstance(): self
    {
        if (!isset(static::$instance)) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    /**
     * @return void
     */
    protected function connection()
    {
        $this->dbh = new \PDO("{$this->config['dsn']}:host={$this->config['host']};dbname={$this->config['dbname']}",
            $this->config['user'],
            $this->config['password']
        );
    }

    public function execute(string $sql, array $sub)
    {
        $this->connection();
        $this->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $stmt = $this->dbh->prepare($sql);
        return $stmt->execute($sub);
    }

    private function __clone()
    {

    }
}