<?php

require './vendor/autoload.php';

array_shift($argv);

if (empty($argv)) {
    exit('Заполните URL адреса' . PHP_EOL);
}

$server = new \components\AmqpSever();

foreach ($argv as $url) {
    $server->sendJob((new \job\SendJob($url, 30)));
}