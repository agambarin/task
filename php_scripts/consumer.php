<?php

require './vendor/autoload.php';

$server = new \components\AmqpSever();

try {
    $server->consumer();
} catch (\Exception $e) {
    $server->closeConnection();
}