<?php

namespace components;

use job\BasicJob;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

class AmqpSever
{
    public $host = 'rabbitmq';
    public $port = 5672;
    public $user = 'guest';
    public $password = 'guest';
    public $queueName = 'queue';
    public $key = 'task';
    public $exchange = 'delayed_exchange';
    public $vhost = '/';

    /**
     * @var AMQPStreamConnection
     */
    protected $connection;

    /**
     * @var AMQPChannel
     */
    protected $channel;

    /**
     * @param string $msg
     * @param int $att
     * @param int $delay
     * @return void
     */
    protected function sendMessage(string $msg, int $att, int $delay = 0)
    {
        $this->openConnection();
        $this->createExchange();
        $this->createQueue();

        $msg = new AMQPMessage("$att,$msg", ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);
        $msg->set('application_headers', new AMQPTable(['x-delay' => $delay]));
        $this->channel->basic_publish($msg, $this->exchange);
    }


    /**
     * @param BasicJob $job
     * @param int $att
     * @return void
     */
    public function sendJob(BasicJob $job, int $att = 1)
    {
        $this->sendMessage(Serializer::serialize($job), $att, $job->getDelayTime());
    }


    /**
     * @return AMQPStreamConnection
     */
    protected function openConnection()
    {
        if ($this->connection) {
            return;
        }

        $this->connection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->password, $this->vhost);
        $this->channel = $this->connection->channel();
    }

    /**
     * @return void
     */
    protected function createExchange()
    {
        $this->channel->exchange_declare($this->exchange,
            'x-delayed-message', false, true, false, false, false,
            new AMQPTable([
                'x-delayed-type' => 'fanout'
            ])
        );
    }

    /**
     * @return void
     */
    protected function createQueue()
    {
        $this->channel->queue_declare($this->queueName, false, false, false, false);
        $this->channel->queue_bind($this->queueName, $this->exchange);
    }

    /**
     * @return void
     * @throws \ErrorException
     */
    public function consumer()
    {
        $this->openConnection();

        $callback = function ($msg) {
            list($att, $job) = explode(',', $msg->body);
            $job = Serializer::unserialize($job);
            $status = $job->run();
            if (!$status && $att < $job->limitAtt()) {
                $job->additionalAttempt(15);
                $this->sendJob($job, $att + 1);
            }
        };

        $this->channel->basic_consume($this->queueName, '', false, false, false, false, $callback);

        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }
    }

    /**
     * @return void
     */
    public function closeConnection()
    {
        $this->connection->close();
        $this->channel->close();
    }
}