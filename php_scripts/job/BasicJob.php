<?php

namespace job;

abstract class BasicJob
{
    /**
     * @var int
     */
    protected $delay = 0;

    abstract public function run();

    public function limitAtt(): int
    {
        return 1;
    }

    public function getDelayTime(): int
    {
        return $this->delay * 1000;
    }

    public function additionalAttempt(int $delay)
    {
        $this->delay = $delay;
    }

}