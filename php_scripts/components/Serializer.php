<?php

namespace components;

use job\BasicJob;

class Serializer
{
    /**
     * @param BasicJob $job
     * @return string
     */
    public static function serialize(BasicJob $job): string
    {
        return serialize($job);
    }

    /**
     * @param string $serialized
     * @return BasicJob
     */
    public static function unserialize(string $serialized): BasicJob
    {
        return unserialize($serialized);
    }
}