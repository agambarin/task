CREATE TABLE response_data
(
    url             TEXT,
    id              INT PRIMARY KEY AUTO_INCREMENT,
    code            SMALLINT,
    response_header JSON,
    body            MEDIUMBLOB,
    date            DATETIME DEFAULT CURRENT_TIMESTAMP
)