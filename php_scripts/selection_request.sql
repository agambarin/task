SELECT COUNT(*) AS 'кол-во',
       (SELECT COUNT(*) FROM response_data AS rd WHERE JSON_EXTRACT(rd.response_header, "$.new[0]") = 1) AS 'поле new'
FROM response_data;