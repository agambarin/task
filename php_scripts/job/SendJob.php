<?php

namespace job;

use components\DbConnection;
use Httpful\Request;

class SendJob extends BasicJob
{
    /**
     * @var string $url
     */
    private string $url;

    public function __construct(string $url, int $delay = 0)
    {
        $this->url = $url;
        $this->delay = $delay;
    }

    /**
     * @return bool
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function run()
    {
        $resp = Request::get($this->url)->send();
        $data = [
            'body' => $resp->raw_body,
            'url' => $this->url,
            'response_header' => json_encode($resp->headers->toArray()),
            'code' => $resp->code
        ];

        $db = DbConnection::getInstance();
        $db->execute("INSERT INTO `response_data` (`url`, `code`, `response_header`, `body`) VALUES (:url, :code, :response_header, :body)", $data);
        return $data['code'] !== 200 ? false : true;
    }

    public function limitAtt(): int
    {
        return 2;
    }
}